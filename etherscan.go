package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"strconv"
	"time"
)

type etherscanClient struct {
	apiKey       string
	transactions []etherscanTransaction
}

type etherscanTransaction struct {
	BlockNumber string
	TimeStamp   string
	Hash        string
}

type etherscanTransactionResponse struct {
	Status  string
	Message string
	Result  []etherscanTransaction
}

func (client *etherscanClient) buildURL(startBlock, endBlock string) string {
	queries := map[string]string{
		"module":          "account",
		"action":          "tokentx",
		"contractaddress": "0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2",
		"address":         "0x88e6a0c2ddd26feeb64f039a2c41296fcb3f5640",
		"sort":            "desc",
		"endblock":        endBlock,
		"startblock":      startBlock,
		//"page":            "1",
		//"offset":          "5",
	}

	api := fmt.Sprintf("https://api.etherscan.io/api?apikey=%s", client.apiKey)
	for k, v := range queries {
		if v != "" {
			api = fmt.Sprintf("%s&%s=%s", api, k, v)
		}
	}

	return api
}

func (client *etherscanClient) fetch(api string) (*[]etherscanTransaction, error) {
	resp, err := http.Get(api)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return nil, fmt.Errorf("HTTP response error")
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Print("Error: could not read body of fetched transactions.")
		return nil, err
	}

	transactionResp := etherscanTransactionResponse{}
	err = json.Unmarshal(body, &transactionResp)
	if err != nil {
		log.Print("Error: could not unmarshal etherscan results.")
		return nil, err
	}

	return &transactionResp.Result, nil
}

func (client *etherscanClient) getTransactions(startTime time.Time) error {
	client.transactions = nil

	startBlock := ""
	endBlock := ""

	for {
		api := client.buildURL(startBlock, endBlock)

		transactionResp, err := client.fetch(api)
		if err != nil {
			return err
		}

		if len(*transactionResp) == 0 {
			break
		}

		startBlock = (*transactionResp)[0].BlockNumber
		for _, item := range *transactionResp {
			endBlock = item.BlockNumber

			timeStamp, err := strconv.ParseInt(item.TimeStamp, 10, 64)
			if err != nil {
				log.Print("Cannot decode block timestamp.")
				continue
			}
			if time.Unix(timeStamp, 0).Before(startTime) {
				return nil
			}
			client.transactions = append(client.transactions, item)
		}
	}
	return nil
}

func (client *etherscanClient) decodeABI() {
	log.Print(client.transactions)
}
