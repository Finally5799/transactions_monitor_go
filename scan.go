package main

import "time"

type scanner interface {
	getTransactions(time.Time) error
	decodeABI()
}
