package main

import (
	"flag"
	"log"
	"os"
	"time"
)

type application struct {
	freq time.Duration
	scan scanner
}

func main() {
	freqFlag := flag.Int("freq", 1, "Frequency for sleeping after processing.")
	chainFlag := flag.String("chain", "ethereum", "The chain to use on, e.g. avalanche, bsc, ethereum, polygon.")
	addressFlag := flag.String("address", "", "The address to monitor (required).")
	sinceFlag := flag.String("since", "", "Start scan beginning from this time")

	flag.Parse()

	address := *addressFlag
	if address == "" {
		flag.Usage()
		os.Exit(2)
	}

	app := application{}
	app.freq = time.Duration(*freqFlag) * time.Second

	chain := *chainFlag
	api_key := os.Getenv("API_KEY")

	switch chain {
	case "ethereum":
		app.scan = &etherscanClient{api_key, nil}
	default:
		app.scan = &etherscanClient{api_key, nil}
	}

	log.Printf("Running on %s chain, monitoring wallet: %s.", chain, address)
	// log.Println(api_key)

	startTime := time.Now().Add(-30 * time.Second)
	if *sinceFlag != "" {
		var err error
		startTime, err = time.Parse(time.RFC3339, *sinceFlag)
		if err != nil {
			log.Print("Time format is wrong, ignoring, use something like, 2000-01-01T13:00:00Z")
			startTime = time.Now()
		}
	}

	app.run(startTime)
}

func (app *application) run(start time.Time) {
	for {
		// Process
		app.scan.getTransactions(start)
		app.scan.decodeABI()

		start = time.Now().Add(-30 * time.Second)
		time.Sleep(app.freq)
	}
}
