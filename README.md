# Transactions Monitor in Go

An exercise to write a simple blockchain transactions monitoring/decoding tool.

## Setup

Create a .env file with the following

    API_KEY=<YOUR_KEY_HERE>

and run setenv.ps1 if on windows


## Requirements

- Provide chain, address to monitor, will probe scans website and decode the events
- Decode the events with the different abis
- Output useful informations